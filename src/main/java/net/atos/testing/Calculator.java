package net.atos.testing;

import java.util.Arrays;
import java.util.LinkedList;

public class Calculator {
    public double evaluate(String operationsSeq) {
        double result = 0;
        String[] operations = operationsSeq.split("");
        LinkedList<String> operationsQueue = new LinkedList<>(Arrays.asList(operations));
        while(!operationsQueue.isEmpty()) {
            result = apply(operationsQueue.pop(), result);
        }
        return result;
    }

    private double apply(String operation, double result) {
        switch (operation) {
            case "+": return result + 1;
            case "-": return result - 1;
            case "*": return result * 2;
            case "/": return result / 2;
        }
        throw new UnknownOperationException();
    }
}
